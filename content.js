import $ from "jquery";
import * as R from "ramda";

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// // pass through events from the page scope to devtools
// window.addEventListener("message", (event) => {
//   if (event.source !== window) {
//     return;
//   }
//   const message = event.data;
//   if (typeof message !== "object" || message === null) {
//     return;
//   }
//   // console.log("pass through events from the page scope to devtools");
//   chrome.runtime.sendMessage(message);
// });

const waitAndGetNode = async (query, text, chances = 5) => {
  const $node = $(query);
  if ($node.length === 1) {
    if (!text) {
      return $node;
    }
    if ($node.text().indexOf(text) > -1) {
      return $node;
    }
  }
  if (chances === 0) {
    return false;
  }
  await delay(300);
  return waitAndGetNode(query, text, chances - 1);
};

// execute the action command from devtools
chrome.runtime.onMessage.addListener(async (msg) => {
  if (msg.cmd) {
    // from dev server
    const $frozen = $(".devbase-frozen");
    if (msg.cmd === "freeze") {
      if ($frozen.length > 0) {
        $("div", $frozen).text(msg.msg);
      } else {
        $(`<div class="devbase-frozen" style="
            position: fixed; top: 0; left: 0; right: 0; bottom: 0;
            z-index: 999999999999; background-color: #00000055;
            display: flex; align-items: center; justify-content: center;
          ">
          <div style="padding: 60px 100px; background-color: white; font-size: 150%;">${msg.msg}</div>
        </div>`).appendTo($("body"));
      }
    } else if (msg.cmd === "unfreeze") {
      $frozen.remove();
    }
    return;
  }
  // console.log(msg);
  const $node = await waitAndGetNode(msg.query, msg.text);
  if (!$node) {
    chrome.runtime.sendMessage({result: "failed"});
    return;
  }
  // assert / click
  if (msg.action === "dblclick") {
    $node[0].dispatchEvent(new MouseEvent("dblclick", {bubbles: true}));
  } else if (msg.action === "click") {
    $node[0].dispatchEvent(new MouseEvent("click", {bubbles: true}));
    $node[0].dispatchEvent(new MouseEvent("mouseup", {bubbles: true}));
  } else if (msg.action === "change") {
    $node.val(msg.val);
    $node[0].dispatchEvent(new Event("change", {bubbles: true}));
  }
  chrome.runtime.sendMessage({result: "passed"});
});

const installObserver = () => {
  // regression test integration
  const getBaseQuery = ($node) => {
    const nodeName = $node[0].nodeName.toLowerCase();
    const classStr = $node.attr("class") || "";
    return classStr === ""
      ? nodeName
      : `${nodeName}.${classStr.split(" ").join(".")}`;
  };

  // prefix: "" | "div.abc>" | ...
  const getQuery = ($node) => {
    let query = getBaseQuery($node);
    let arr = $(query).toArray();
    let $current = $($node);
    /* eslint-disable no-restricted-syntax */
    for (const item of [1, 2, 3]) {
      if (arr.length < 30) {
        break;
      }
      $current = $current.parent();
      if ($current.length === 0) {
        break;
      }
      query = `${getBaseQuery($current)}>${query}`;
      arr = $(query).toArray();
    }
    const idx = arr.indexOf($node[0]);
    return `${query}:nth(${idx})`;
  };

  const optimizeQuery = (initial) => {
    const node = $(initial)[0];
    const [query, nthPart] = initial.split(":");
    const matches = query.match(/\./g);
    if (!matches) {
      return initial;
    }
    const optimized = R.pipe(
      R.times(R.identity),
      R.reverse,
      R.map((i) => {
        const parts = query.split(/(?=\.)/);
        const q =
          parts[i + 1].indexOf(">") > -1
            ? R.update(i + 1, parts[i + 1].split(">")[1], parts).join("")
            : R.remove(i + 1, 1, parts).join("");
        return `${q}:${nthPart}`;
      }),
      R.find((q) => $(q)[0] === node)
    )(matches.length);
    if (optimized) {
      return optimizeQuery(optimized);
    }
    return initial;
  };

  const reinstallListener = () => {
    // console.log('reinstalled');
    let lastTimestamp = false;
    const onEvent = (e, callback) => {
      const event = e.originalEvent;
      if (
        event &&
        event.isTrusted &&
        !event.processed &&
        event.timeStamp > lastTimestamp
      ) {
        lastTimestamp = event.timeStamp;
        event.processed = true;
        const $node = $(e.currentTarget);
        const query = optimizeQuery(getQuery($node));
        // window.postMessage(callback($node, query), "*");
        chrome.runtime.sendMessage(callback($node, query));
      }
    };
    $(
      "input:not([type='checkbox']):not([type='radio']):not([type='button']),select,textarea"
    )
      .off("change.observe-change")
      .on("change.observe-change", (e) => {
        onEvent(e, ($node, query) => {
          return {
            captured: {
              action: "change",
              query,
              val: $node.val(),
            },
          };
        });
      });
    $("*:not(svg):not(use)")
      .off("mousedown.observe-click")
      .on("mousedown.observe-click", (e) => {
        if ($(e.target).is("input,select,textarea")) {
          return;
        }
        onEvent(e, ($node, query) => {
          return {
            captured: {
              action: "click",
              query,
              text: $node.text(),
            },
          };
        });
      });
    $("*:not(svg):not(use)")
      .off("dblclick.observe-dblclick")
      .on("dblclick.observe-dblclick", (e) => {
        if ($(e.target).is("input,select,textarea")) {
          return;
        }
        onEvent(e, ($node, query) => {
          return {
            captured: {
              action: "dblclick",
              query,
              text: $node.text(),
            },
          };
        });
      });
  };
  const observer = new MutationObserver(reinstallListener);
  observer.observe($("body")[0], {
    attributes: true,
    characterData: true,
    childList: true,
    subtree: true,
  });
  $("body").on("statechanged", reinstallListener);
  reinstallListener();
};

$(() => {
  chrome.runtime.sendMessage(
    {askIfNeedToInstallObservers: 1},
    {},
    (response) => {
      if (response === "yes") {
        console.log("install observer ...");
        installObserver();
      }
    }
  );
});
