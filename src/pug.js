import * as R from "ramda";

/* eslint-disable fp/no-rest-parameters */
/* eslint-disable no-restricted-syntax */
const interpolate = (literals, expressions) => {
  let string = ``;
  for (const [i, val] of expressions.entries()) {
    string += literals[i] + val;
  }
  string += literals[literals.length - 1];
  return string;
};

const trimPaddingSpaces = (string) => {
  const lines = string.split("\n").slice(1, -1);
  const nSpaces = lines[0].match("^ *")[0].length;
  return R.pipe(
    R.map((line) => line.slice(nSpaces)),
    R.join("\n")
  )(lines);
};

const trim = (literals, ...expressions) => {
  const string = interpolate(literals, expressions);
  return trimPaddingSpaces(string);
};

export const pugCompile = (literals, ...expressions) => {
  let string = interpolate(literals, expressions);
  string = trimPaddingSpaces(string);
  return pug.compile(string);
};
