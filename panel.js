import * as R from "ramda";
import {debounce} from "throttle-debounce";
import {pugCompile} from "./src/pug";

const log = (msg) =>
  $(".log").text(R.is(String, msg) ? msg : JSON.stringify(msg));

const getCurrentTab = () =>
  new Promise((resolve) => {
    chrome.tabs.query({active: true, lastFocusedWindow: true}, (tabs) => {
      resolve(tabs[0]);
    });
  });

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const captureOneNode = () =>
  new Promise((resolve) => {
    const listener = (msg, sender, sendResponse) => {
      if (msg.captured) {
        chrome.runtime.onMessage.removeListener(listener);
        resolve(msg.captured);
      }
    };
    chrome.runtime.onMessage.addListener(listener);
  });

const waitForTab = (targetTabId) =>
  new Promise((resolve) => {
    const listener = (tabId, changeInfo, tab) => {
      if (tabId === targetTabId && changeInfo.status === "complete") {
        chrome.tabs.onUpdated.removeListener(listener);
        resolve();
      }
    };
    chrome.tabs.onUpdated.addListener(listener);
  });

const handlers = {
  async sleep(item) {
    await delay(item.intervalMS);
    return true;
  },

  async resetdb(_, currentTab) {
    const host = currentTab.url.split("/")[2];
    const url = `https://${host}/_/resetdb`;
    await fetch(url, {
      cache: "no-cache",
      method: "POST",
    });
    await delay(100);
    chrome.tabs.reload(currentTab.id);
    await waitForTab(currentTab.id);
    return true;
  },

  async goto(item, currentTab) {
    if (currentTab.url === item.href) {
      return true;
    }
    chrome.tabs.update(currentTab.id, {url: item.href});
    await waitForTab(currentTab.id);
    return true;
  },

  async assertUrl(item, currentTab) {
    await Promise.race([waitForTab(currentTab.id), delay(2000)]);
    await delay(300);
    const tab = await getCurrentTab();
    return tab.url === item.href;
  },
};

const executeTestPlan = async (actions, noHighlight = false) => {
  log("Executing test ...");
  const updateState = ($btn, state) => {
    if (!noHighlight) {
      $btn.attr("state", state);
    }
  };
  $(`.btn[data-execute-step]`).removeAttr("state");
  const currentTab = await getCurrentTab();
  /* eslint-disable no-restricted-syntax */
  /* eslint-disable no-await-in-loop */
  for (const [idx, item] of actions.entries()) {
    const $btn = $(`.btn[data-execute-step="${idx}"]`);
    updateState($btn, "running");
    if (handlers[item.action]) {
      const result = await handlers[item.action](item, currentTab);
      if (result) {
        updateState($btn, "passed");
      } else {
        updateState($btn, "failed");
        log("Test step failed.");
        return false;
      }
    } else {
      const waitFor = Promise.race([
        new Promise((resolve) => {
          const listener = (msg, sender, sendResponse) => {
            // result of step execution
            if (msg.result) {
              chrome.runtime.onMessage.removeListener(listener);
              resolve(msg.result);
            }
          };
          chrome.runtime.onMessage.addListener(listener);
        }),
        delay(5000),
      ]);
      chrome.tabs.sendMessage(currentTab.id, item);
      const result = await waitFor;
      updateState($btn, result);
      if (result === "failed") {
        log("Test step failed.");
        return false;
      }
    }
  }
  log("All steps passed.");
  return true;
};

const getJson = async (url) => {
  const res = await fetch(url);
  return res.json();
};

$(async () => {
  const currentTab = await getCurrentTab();
  const host = currentTab.url.split("/")[2];

  const getPlans = async () => {
    try {
      log("Downloading ...");
      const plans = await getJson(`https://${host}/_/test-plans`);
      log("Loaded test plans.");
      return R.pipe(R.indexBy(R.prop("name")), R.pluck("steps"))(plans);
    } catch (err) {
      log("Failed to download test plans.");
      return {};
    }
  };

  let plans = await getPlans();
  let planName = false;
  let capturing = false;
  let runOnReload = false;

  const renderTestPlans = () => {
    $(".canvas").html(
      pugCompile`
        .flex-r-jfs-ac
          .btn.btn-reset-db reset db
          .btn.btn-new-test.ml1
            i.fa.fa-plus
          .btn(data-run-all-plans).ml1
            i.fa.fa-play
          .btn.btn-save.ml1
            i.fa.fa-save
          .btn(data-refresh-list).ml1
            i.fa.fa-refresh
        .pt2 Test cases:
        each name in planNames
          .flex-r-jfs-ac.mts
            .btn(data-delete-plan=name)
              i.fa.fa-close
            .btn.ml1(data-rename-plan=name)
              i.fa.fa-tag
            .btn.ml1(data-run-plan=name)
              i.fa.fa-play
            .btn.ml1(data-open-plan=name)= name
      `({planNames: R.sortBy(R.identity, R.keys(plans))})
    );
  };

  const renderOneTestPlan = () => {
    const steps = plans[planName];
    $(".canvas").html(
      pugCompile`
        .flex-r-jfs-ac
          .btn.btn-list
            i.fa.fa-backward
          .btn.btn-reset-db.ml1 reset db
          .btn.btn-test.ml1
            i.fa.fa-play
          .btn.btn-save.ml1
            i.fa.fa-save
          .btn.btn-toggle-run-on-reload.ml1(style=runOnReload ? "" : "color: #555")
            i.fa.fa-retweet
        .pt2 #{steps.length} steps of [${planName}]:
        each step, idx in steps
          .flex-r-jfs-ac.mts
            .btn(data-delete-step=idx)
              i.fa.fa-trash
            .btn.ml1(data-execute-upto=idx)
              i.fa.fa-step-forward
            .btn.ml1(data-execute-step=idx)= step.action
            .ml1(
              data-edit-step=idx
              style="cursor: pointer; overflow: hidden; white-space: nowrap; text-overflow: ellipsis"
            )
              if step.action === 'assert' || step.action === 'click' || step.action === "dblclick"
                | #{step.text} :: #{step.query}
              else if step.action === 'change'
                | #{step.val} :: #{step.query}
              else
                | #{step.href || step.intervalMS}
        .pt2 Add:
        .flex-r-jfs-ac.py1
          .btn.btn-toggle-capture= capturing ? 'stop' : 'capture'
          .btn.ml1(data-add-step="assert") assert
          .btn.ml1(data-add-step="assertUrl") assertUrl
          .btn.ml1(data-add-step="sleep") sleep
          .btn.ml1(data-add-step="goto") goto
          .btn.ml1(data-add-step="resetdb") resetdb
      `({planName, steps, capturing, runOnReload})
    );
  };

  chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    if (msg.askIfNeedToInstallObservers) {
      const response = sender.tab.id === currentTab.id ? "yes" : "no";
      sendResponse(response);
      return;
    }
    // result of step execution
    if (msg.result) {
      return;
    }
    if (capturing === false) return;
    /* eslint-disable fp/no-mutating-methods */
    plans[planName].push(msg.captured);
    renderOneTestPlan();
    $(".log").text(JSON.stringify(msg));
  });

  const stopRunningCommands = () => {
    capturing = false;
    renderOneTestPlan();
  };

  $("body").on("click.refresh-list", "[data-refresh-list]", async () => {
    plans = await getPlans();
    planName = false;
    renderTestPlans();
  });

  // curl -XPOST https://dash.pointup-dev.com/_/resetdb
  $("body").on("click.reset-db", ".btn-reset-db", async () => {
    const tab = await getCurrentTab();
    await handlers.resetdb({}, tab);
  });

  $("body").on("click.run-test", ".btn-test", async () => {
    stopRunningCommands();
    await executeTestPlan(plans[planName]);
  });

  $("body").on(
    "click.run-toggle-run-on-reload",
    ".btn-toggle-run-on-reload",
    async () => {
      runOnReload = !runOnReload;
      renderOneTestPlan();
      if (runOnReload) {
        log("Auto run on reload: ON");
      } else {
        log("Auto run on reload: OFF");
      }
    }
  );

  const runPlanOnListPage = async (name) => {
    const $btn = $(`[data-run-plan="${name}"]`);
    $btn.attr("state", "running");
    const r = await executeTestPlan(plans[name], true);
    $btn.attr("state", r ? "passed" : "failed");
  };

  $("body").on("click.run-plan", "[data-run-plan]", async (e) => {
    $(`[data-run-plan]`).removeAttr("state");
    await runPlanOnListPage($(e.currentTarget).data("run-plan"));
  });

  $("body").on("click.run-all-plans", "[data-run-all-plans]", async (e) => {
    $(`[data-run-plan]`).removeAttr("state");
    const ordered = R.sortBy(R.identity, R.keys(plans));
    for (const name of ordered) {
      await runPlanOnListPage(name);
    }
  });

  $("body").on("click.toggle-capture", ".btn-toggle-capture", () => {
    capturing = !capturing;
    if (capturing) {
      log("Capturing, please interact with the webpage normally.");
    }
    renderOneTestPlan();
  });

  const stepAdders = {
    async assert() {
      log("Capturing, please click on the asserting node.");
      const captured = await captureOneNode();
      const step = R.assoc("action", "assert", captured);
      plans[planName].push(step);
    },
    async assertUrl() {
      const tab = await getCurrentTab();
      plans[planName].push({action: "assertUrl", href: tab.url});
    },
    async sleep() {
      plans[planName].push({action: "sleep", intervalMS: "200"});
    },
    async goto() {
      const tab = await getCurrentTab();
      plans[planName].push({action: "goto", href: tab.url});
    },
    async resetdb() {
      plans[planName].push({action: "resetdb"});
    },
  };

  const stepUpdaters = {
    async sleep(step) {
      const intervalMS = prompt("Interval (ms)", step.intervalMS) || "";
      if (intervalMS === "") return false;
      step.intervalMS = intervalMS;
    },
    async assertUrl(step) {
      const tab = await getCurrentTab();
      step.href = tab.url;
    },
    async goto(step) {
      const tab = await getCurrentTab();
      step.href = tab.url;
    },
    async click(step) {
      step.text = "capturing ...";
      step.query = "capturing ...";
      renderOneTestPlan();
      const captured = await captureOneNode();
      step.query = captured.query;
      step.text = captured.text;
    },
  };

  $("body").on("click.add-step", "[data-add-step]", async (e) => {
    stopRunningCommands();
    const $btn = $(e.currentTarget);
    const action = $btn.data("add-step");
    const adder = stepAdders[action];
    if (adder) {
      await adder();
      renderOneTestPlan();
    }
  });

  $("body").on("click.save", ".btn-save", async () => {
    log("Saving ...");
    const url = `https://${host}/_/test-plans/save`;
    const plansArr = R.pipe(
      R.toPairs,
      R.map(([name, steps]) => ({name, steps})),
      R.sortBy(R.prop("name"))
    )(plans);
    const res = await fetch(url, {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({plans: plansArr}),
    });
    const json = await res.json();
    if (json.ok === 1) {
      log("Saved.");
    }
  });

  $("body").on("click.list-tests", ".btn-list", async () => {
    stopRunningCommands();
    renderTestPlans();
  });

  $("body").on("click.execute-step", "[data-execute-upto]", async (e) => {
    stopRunningCommands();
    const $btn = $(e.currentTarget);
    const stepIdx = $btn.data("execute-upto");
    const steps = R.take(stepIdx + 1, plans[planName]);
    await executeTestPlan(steps);
  });

  $("body").on("click.edit-step", "[data-edit-step]", async (e) => {
    stopRunningCommands();
    const $btn = $(e.currentTarget);
    const stepIdx = $btn.data("edit-step");
    const step = plans[planName][stepIdx];
    const updater = stepUpdaters[step.action];
    if (updater) {
      await updater(step);
    }
    renderOneTestPlan();
  });

  $("body").on("click.execute-step", "[data-execute-step]", async (e) => {
    stopRunningCommands();
    const $btn = $(e.currentTarget);
    const stepIdx = $btn.data("execute-step");
    const step = plans[planName][stepIdx];
    await executeTestPlan([step], true);
  });

  $("body").on("click.delete-step", "[data-delete-step]", async (e) => {
    stopRunningCommands();
    const $btn = $(e.currentTarget);
    const stepIdx = $btn.data("delete-step");
    plans[planName] = R.remove(stepIdx, 1, plans[planName]);
    renderOneTestPlan();
  });

  $("body").on("click.open-plan", "[data-open-plan]", async (e) => {
    const $btn = $(e.currentTarget);
    planName = $btn.data("open-plan");
    renderOneTestPlan();
  });

  $("body").on("click.delete-plan", "[data-delete-plan]", async (e) => {
    const $btn = $(e.currentTarget);
    const delName = $btn.data("delete-plan");
    plans = R.dissoc(delName, plans);
    renderTestPlans();
  });

  $("body").on("click.rename-plan", "[data-rename-plan]", async (e) => {
    const $btn = $(e.currentTarget);
    const oldName = $btn.data("rename-plan");
    const newName = prompt(`New name of [${oldName}]?`) || "";
    if (newName === "") return false;
    if (R.has(newName, plans)) alert("Name already used");
    plans = R.pipe(R.assoc(newName, plans[oldName]), R.dissoc(oldName))(plans);
    renderTestPlans();
  });

  $("body").on("click.new-test", ".btn-new-test", async (e) => {
    const newName = prompt("Name of the test plan?") || "";
    if (newName === "") return false;
    if (R.has(newName, plans)) alert("Name already used");
    plans = R.assoc(newName, [], plans);

    planName = newName;
    await stepAdders.resetdb();
    await stepAdders.goto();
    renderOneTestPlan();
  });

  renderTestPlans();

  const socket = io(`https://${host}`, {
    path: "/dev-server",
    transports: ["websocket"], // disabled polling
  });
  socket.on("connect", () => $(".devserver-status").text("connected"));
  socket.on("disconnect", () => $(".devserver-status").text("connecting ..."));

  // debounce, and not at begin
  const reloadTab = debounce(300, false, async (msg) => {
    chrome.tabs.sendMessage(currentTab.id, {cmd: "freeze", msg: msg.reason});
    chrome.tabs.reload(currentTab.id);
    if (runOnReload && planName) {
      await waitForTab(currentTab.id);
      await executeTestPlan(plans[planName]);
    }
  });

  socket.on("msg", async (msg) => {
    log(`dev-server ${JSON.stringify(msg)}`);

    if (msg.cmd === "freeze") {
      chrome.tabs.sendMessage(currentTab.id, {cmd: "freeze", msg: msg.reason});
    } else if (msg.cmd === "unfreeze") {
      chrome.tabs.sendMessage(currentTab.id, {
        cmd: "unfreeze",
        msg: msg.reason,
      });
    } else if (msg.cmd === "reload") {
      reloadTab(msg);
    }
  });
});
