airi_frontend
=============

Install direnv to source the development environment, and allow for this project

```
apt-get install direnv
direnv allow .
```

Source the vim settings, add in your VimRC

```
au VimEnter * call LoadProjectVimRC()
function! LoadProjectVimRC()
  if filereadable(".devbase/project.vim")
    so .devbase/project.vim
  endif
endfunction
```

Start the app with `reboot-dev` or `<leader>r` in Vim

Open console with `console` or `co` in Vim

`yarn install`

Restart the UID and asset compiler again with `reboot-dev` or `<leader>r` in Vim

In Vim, `sl` to view js compile logs.

In Vim, `<leader>ou` to open uid.

`cleanup-all-images` to clean up the dev. docker images.
